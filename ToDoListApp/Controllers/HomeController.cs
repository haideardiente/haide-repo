﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using ToDoListApp.DAL;
using ToDoListApp.Models;
using ToDoListApp.Util;
using System.Data.SqlTypes;

namespace ToDoListApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var session = this.Session["current_list"] as TaskListViewModel;
            if (session != null)
            {
                using (var db = new DBContext())
                {
                    var query = db.TaskLists.Find(session.Id);
                    if (query != null)
                    {
                        var taskListViewModel = new TaskListViewModel
                        {
                            Id = query.id,
                            Title = query.Title,
                            DateCreated = query.DateCreated,
                            Tasks = query.Tasks.ToList()
                        };

                        var model = new ListViewModel
                        {
                            TaskListViewModel = taskListViewModel,
                            NewTaskViewModel = new TaskViewModel { TaskListId = query.id }
                        };

                        return View(model);
                    }
                }
            }
            return RedirectToAction("New");
        }

        public ActionResult New()
        {
            return View();
        }

        [Route("~/")]
        [Route]
        [Route("Index")]
        public ActionResult DeleteTask(int id)
        {
            using (var db = new DBContext())
            {
                var task = db.Tasks.Find(id);
                if (task != null)
                {
                    var taskList = task.TaskList;
                    var taskListViewModel = new TaskListViewModel
                    {
                        Id = taskList.id
                    };

                    db.Tasks.Remove(task);
                    db.SaveChanges();

                    taskListViewModel.Tasks = taskList.Tasks.ToList();

                    return PartialView("_TaskList", taskListViewModel);
                }
            }

            return PartialView("_Error", new MessagePartialViewModel("Error deleting task"));
        }

        [Route("~/")]
        [Route]
        [Route("Index")]
        [HttpPost]
        public ActionResult UpdateName(int id, string newTitle)
        {
            using (var db = new DBContext())
            {
                var task = db.Tasks.Find(id);
                if (task != null)
                {
                    var taskList = task.TaskList;
                    var taskListViewModel = new TaskListViewModel
                    {
                        Id = taskList.id
                    };

                    task.TaskName = newTitle;
                    db.Tasks.Attach(task);
                    db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    taskListViewModel.Tasks = taskList.Tasks.ToList();

                    return PartialView("_TaskList", taskListViewModel);
                }
            }

            return PartialView("_Error", new MessagePartialViewModel("Error updating task name"));
        }

        [Route("~/")]
        [Route]
        [Route("Index")]
        public ActionResult CompleteTask(int id)
        {
            try
            {
                using (var db = new DBContext())
                {
                    var task = db.Tasks.Find(id);
                    if (task != null)
                    {
                        var taskList = task.TaskList;
                        var taskListViewModel = new TaskListViewModel
                        {
                            Id = taskList.id
                        };

                        task.IsCompleted = !task.IsCompleted;
                        task.DateCompleted = task.IsCompleted ? DateTime.Now : SqlDateTime.MinValue.Value;
                        db.Tasks.Attach(task);
                        db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        taskListViewModel.Tasks = taskList.Tasks.ToList();

                        return PartialView("_TaskList", taskListViewModel);
                    }
                }

                return PartialView("_Error", new MessagePartialViewModel("Error completing task"));
            }
            catch (Exception e)
            {
                throw;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("~/")]
        [Route]
        [Route("Index")]
        public ActionResult NewTask(TaskViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newTask = new Task
                    {
                        TaskName = model.TaskDescription,
                        DateCreated = DateTime.Now,
                        IsCompleted = false,
                    };
                    using (var db = new DBContext())
                    {
                        var taskList = db.TaskLists.Find(model.TaskListId);
                        if (taskList != null)
                        {
                            newTask.TaskList = taskList;
                            db.Tasks.Add(newTask);
                            db.SaveChanges();

                            var taskListViewModel = new TaskListViewModel
                            {
                                Id = taskList.id,
                                Tasks = taskList.Tasks.ToList()
                            };

                            return PartialView("_TaskList", taskListViewModel);
                        }
                    }
                }
                return View("Index", model);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                return View("Index", model);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewList(TaskListSimpleViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var sessionKey = Helpers.RandomString(12);

                    var newTask = new Task
                    {
                        TaskName = model.Title,
                        DateCreated = DateTime.Now,
                        IsCompleted = false,
                    };

                    var newTaskList = new TaskList
                    {
                        Title = model.Title,
                        DateCreated = DateTime.Now,
                        SessionKey = sessionKey
                    };

                    newTaskList.Tasks.Add(newTask);

                    using (var db = new DBContext())
                    {
                        db.Tasks.Add(newTask);
                        db.TaskLists.Add(newTaskList);
                        db.SaveChanges();
                    }

                    var taskListViewModel = new TaskListViewModel
                    {
                        Id = newTaskList.id,
                        Title = model.Title,
                        DateCreated = newTaskList.DateCreated,
                        Tasks = new List<Task>()
                    };
                    taskListViewModel.Tasks.Add(newTask);
                    Session["current_list"] = taskListViewModel;
                    return RedirectToAction("Index");
                }

                return View("New", model);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                return View("New", model);
            }
        }

    }
}