﻿$(function () {
    //submit form on Enter key
    $("#TaskDescription").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $('#btnAddNewTask').trigger('click');

            $("#TaskDescription").val('');
        }
    });

    $("body #tasks").on("click", ".btnDelete", function (e) {
        var url = $(this).attr("href");
        e.preventDefault();

        $.ajax({
            url: url,
            contentType: 'application/html',
            success: function (content) {
                $("#tasks").html(content);
            },
            error: function (e) { }
        });
    });

    $("body #tasks").on("click", ".btnCompleteTask", function (e) {

        var url = $(this).attr("href");
        e.preventDefault();

        $.ajax({
            url: url,
            type: "POST",
            contentType: 'application/html',
            success: function (content) {
                $("#tasks").html(content);
            },
            error: function (e) { }
        });
    });
    //$('.clickedit').hide()
    //    .focusout(endEdit)
    //    .keyup(function (e) {
    //        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
    //            endEdit(e);
    //            return false;
    //        } else {
    //            return true;
    //        }
    //    })
    //    .prev().click(function () {
    //        $(this).hide();
    //        $(this).next().show().focus();
    //    });
});