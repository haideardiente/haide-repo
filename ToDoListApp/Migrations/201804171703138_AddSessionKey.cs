namespace ToDoListApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSessionKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskList", "SessionKey", c => c.String());
            DropColumn("dbo.TaskList", "CreatedBy");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TaskList", "CreatedBy", c => c.String());
            DropColumn("dbo.TaskList", "SessionKey");
        }
    }
}
