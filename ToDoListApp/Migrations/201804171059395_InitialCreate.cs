namespace ToDoListApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskList",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        TaskName = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        DateCompleted = c.DateTime(),
                        DueDate = c.DateTime(),
                        Order = c.Int(),
                        TaskList_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.TaskList", t => t.TaskList_id)
                .Index(t => t.TaskList_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Task", "TaskList_id", "dbo.TaskList");
            DropIndex("dbo.Task", new[] { "TaskList_id" });
            DropTable("dbo.Task");
            DropTable("dbo.TaskList");
        }
    }
}
