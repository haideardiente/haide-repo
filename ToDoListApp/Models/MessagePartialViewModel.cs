﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApp.Models
{
    public class MessagePartialViewModel
    {
        public string Message { get; set; }
        public string Url { get; set; }

        public MessagePartialViewModel(string message, string url = null)
        {
            Message = message;
            Url = url;
        }
    }
}