﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApp.Models
{
    public class TaskList
    {
        public TaskList()
        {
            Tasks = new List<Task>();
        }
        public int id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public string SessionKey { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}