﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApp.Models
{
    public class Task
    {
        public int id { get; set; }
        public string TaskName { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Order { get; set; }

        public virtual TaskList TaskList { get; set; }
    }
}