﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoListApp.Models
{
    public class ListViewModel
    {
        public TaskListViewModel TaskListViewModel { get; set; }

        public TaskViewModel NewTaskViewModel { get; set; }
    }

    public class TaskListSimpleViewModel
    {
        [Display(Name = "Add your first task")]
        [Required]
        public string Title { get; set; }
    }

    public class TaskListViewModel
    {
        public TaskListViewModel()
        {
            Tasks = new List<Task>();
            CompletedTasks = new List<Task>();
            InProgresstTasks = new List<Task>();
        }
        [Display(Name = "Add your first task")]
        [Required]
        public string Title { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Task> CompletedTasks
        {
            get
            {
                if (Tasks.Any())
                {
                    return Tasks.Where(x => x.IsCompleted).ToList();
                }
                return new List<Task>();
            }
            set { }
        }
        public List<Task> InProgresstTasks
        {
            get
            {
                if (Tasks.Any())
                {
                    return Tasks.Where(x => !x.IsCompleted).ToList();
                }
                return new List<Task>();
            }
            set { }
        }
        public DateTime DateCreated { get; set; }
        public int Id { get; set; }

    }
    public class TaskViewModel
    {
        [Required]
        [Display(Name = "Add your task description")]
        public string TaskDescription { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Order { get; set; }
        public int TaskListId { get; set; }

    }
}